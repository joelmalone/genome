using System;

namespace Genome
{
    public class MemberChromosome<TGenome, TData> : IChromosome<TGenome>
    {

        private readonly Func<TGenome, TData> _chromosomeDataGetter;
        private readonly Action<TGenome, TData> _chromosomeDataSetter;
        private readonly Func<TData> _randomiser;

        public MemberChromosome(Func<TGenome, TData> chromosomeDataGetter, Action<TGenome, TData> chromosomeDataSetter, Func<TData> randomiser)
        {
            _chromosomeDataGetter = chromosomeDataGetter;
            _chromosomeDataSetter = chromosomeDataSetter;
            _randomiser = randomiser;
        }

        public void SetRandom(TGenome genome)
        {
            var data = _randomiser();
            _chromosomeDataSetter(genome, data);
        }

        public void CopyFrom(TGenome from, TGenome to)
        {
            var data = _chromosomeDataGetter(from);
            _chromosomeDataSetter(to, data);
        }

    }
}