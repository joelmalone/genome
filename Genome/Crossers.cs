using System;
using System.Collections.Generic;

namespace Genome
{
    public interface ICrosser<TGenome>
    {
        void Cross(IEnumerable<IChromosome<TGenome>> chromosomes, TGenome left, TGenome right, TGenome target);
    }

    public class RandomChanceEither<TGenome> : ICrosser<TGenome>
    {
        public void Cross(IEnumerable<IChromosome<TGenome>> chromosomes, TGenome left, TGenome right, TGenome target)
        {
            foreach (var chromosome in chromosomes)
            {
                var source = ThreadLocalRandom.NextDouble() < .5 ? left : right;
                chromosome.CopyFrom(source, target);
            }
        }
    }

    public class RandomChanceEitherWithMutation<TGenome> : ICrosser<TGenome>
    {
        private readonly double _mutationRate;
        public RandomChanceEitherWithMutation(double mutationRate = .05)
        {
            _mutationRate = mutationRate;
        }

        public void Cross(IEnumerable<IChromosome<TGenome>> chromosomes, TGenome left, TGenome right, TGenome target)
        {
            foreach (var chromosome in chromosomes)
            {
                if (ThreadLocalRandom.NextDouble() < _mutationRate)
                {
                    chromosome.SetRandom(target);
                }
                else
                {
                    var source = ThreadLocalRandom.NextDouble() < _mutationRate ? left : right;
                    chromosome.CopyFrom(source, target);
                }
            }
        }

    }
}