﻿using System;
using System.Collections.Generic;

namespace Genome
{
    public interface IGenome
    {
        double Fitness { get; set; }
    }

    public static class GenomeExtensions
    {

        public static TGenome ComputeFitness<TGenome>(this TGenome me, Func<TGenome, double> fitnessFunc) where TGenome : IGenome
        {
            var fitness = fitnessFunc(me);
            me.Fitness = fitness;
            return me;
        }

        public static TGenome PopulateFromCross<TGenome>(
            this TGenome me,
            IEnumerable<IChromosome<TGenome>> chromosomes,
            TGenome left,
            TGenome right,
            ICrosser<TGenome> crosser)
        {
            crosser.Cross(chromosomes, left, right, me);
            return me;
        }
    }

}