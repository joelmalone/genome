﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genome
{
    
    public static class IListExtensions
    {

        public static T Pick<T>(this IList<T> items, double normalisedIndex)
        {
            var index = (int) (items.Count*normalisedIndex);
            var result = items[index];
            return result;
        }

    }
}
