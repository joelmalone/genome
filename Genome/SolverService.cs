﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Genome
{

    public class SolverService<TGenome> where TGenome : IGenome
    {

        public Func<TGenome> GenomeInstantiator { get; set; }
        public IEnumerable<IChromosome<TGenome>> Chromosomes { get; set; }
        public Func<IList<TGenome>, TGenome> Selector { get; set; }
        public ICrosser<TGenome> Crosser { get; set; }

        public IEnumerable<TGenome> InitialiseNewGenomes(int length)
        {
            var result = from i in Enumerable.Range(0, length).AsParallel()
                         select InitialiseNewGenome();
            return result;
        }

        public TGenome InitialiseNewGenome()
        {
            var genome = GenomeInstantiator();
            foreach (var chromosome in Chromosomes)
            {
                chromosome.SetRandom(genome);
            }
            return genome;
        }

        public IEnumerable<TGenome[]> Solve(int setSize, Func<TGenome, double> fitnessFunc)
        {
            if (Chromosomes == null)
                throw new InvalidOperationException();
            if (!Chromosomes.Any())
                throw new InvalidOperationException();
            if (Selector == null)
                throw new ArgumentNullException();
            if (Crosser == null)
                throw new ArgumentNullException();

            var orderedSet = (
                from i in Enumerable.Range(0, setSize).AsParallel()
                let genome = InitialiseNewGenome()
                    .ComputeFitness(fitnessFunc)
                orderby genome.Fitness descending
                select genome
                ).ToArray();

            while (true)
            {
                // ReSharper disable AccessToModifiedClosure
                //  because we're executing immediately
                orderedSet = (
                    from i in Enumerable.Range(0, setSize).AsParallel()
                    let parent1 = Selector(orderedSet)
                    let parent2 = Selector(orderedSet)
                    let child = GenomeInstantiator()
                        .PopulateFromCross(Chromosomes, parent1, parent2, Crosser)
                        .ComputeFitness(fitnessFunc)
                    orderby child.Fitness descending
                    select child
                    ).ToArray();
                // ReSharper restore AccessToModifiedClosure

                yield return orderedSet;
            }
        }

    }
}
