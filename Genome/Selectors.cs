using System;
using System.Collections.Generic;

namespace Genome
{
    public static class Selectors
    {

        public static T CubedWeighted<T>(IList<T> list)
        {
            var length = list.Count;
            // Favour results at the better end of the list
            var r = ThreadLocalRandom.NextDouble() * ThreadLocalRandom.NextDouble() * ThreadLocalRandom.NextDouble();
            var index = (int)(r * length);
            return list[index];
        }

    }
}