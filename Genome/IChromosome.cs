﻿namespace Genome
{
    public interface IChromosome<in TGenome>
    {
        void SetRandom(TGenome genome);
        void CopyFrom(TGenome from, TGenome to);
    }
}