﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Genome;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Genome.Tests
{
    [TestClass]
    public class SolverServiceTests
    {
        [TestMethod]
        public void TestInitialise()
        {
            var chromosomes = new[]
            {
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastOrientation,
                    (g,d) => g.BlastOrientation = d,
                    () => 37
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastScale,
                    (g,d) => g.BlastScale = d,
                    () => 43
                    ),
            };
            var service = new SolverService<PitReserverGenome>
            {
                GenomeInstantiator = () => new PitReserverGenome(),
                Chromosomes = chromosomes,
            };
            var genomes = service.InitialiseNewGenomes(100000);

            var array = genomes.ToArray();
            Assert.AreEqual(100000, array.Length);
            Assert.AreEqual(37, array[3].BlastOrientation);
            Assert.AreEqual(43, array[7].BlastScale);
        }

        /**
         * Test disabled because the randomness is not deterministic; 
         * can re-enable if we refactor the design to use a RandomFactory 
         * or something.
         */
        [Ignore]
        [TestMethod]
        public void TestCross()
        {
            var chromosomes = new[]
            {
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastOrientation,
                    (g,d) => g.BlastOrientation = d,
                    () => 0
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastScale,
                    (g,d) => g.BlastScale = d,
                    () => 0
                    ),
            };
            var genome1 = new PitReserverGenome
            {
                BlastOrientation = 7,
                BlastScale = 17,
            };
            var genome2 = new PitReserverGenome
            {
                BlastOrientation = 13,
                BlastScale = 23,
            };
            var crosser = new RandomChanceEither<PitReserverGenome>();
            var target = new PitReserverGenome();
            crosser.Cross(chromosomes, genome1, genome2, target);

            Assert.AreEqual(7, target.BlastOrientation);
            Assert.AreEqual(23, target.BlastScale);
        }

        [TestMethod]
        public void TestSolveForPosition()
        {
            const double optimalX = 1500;
            const double optimalY = -3500;
            const double optimalAlt = 50;
            const double maxDistSquared = 5000 * 5000 + 5000 * 5000 + 250 * 250;
            const double optimalOrientation = 135;
            const double optimalScale = .85;

            var chromosomes = new[]
            {
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastX,
                    (g,d) => g.BlastX = d,
                    () => ThreadLocalRandom.NextDouble() * 10000 - 5000
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastY,
                    (g,d) => g.BlastY = d,
                    () => ThreadLocalRandom.NextDouble() * 10000 - 5000
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastAlt,
                    (g,d) => g.BlastAlt = d,
                    () => ThreadLocalRandom.NextDouble() * 500 - 250
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastOrientation,
                    (g,d) => g.BlastOrientation = d,
                    () => ThreadLocalRandom.NextDouble() * 360
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastScale,
                    (g,d) => g.BlastScale = d,
                    ThreadLocalRandom.NextDouble
                    ),
            };

            Func<PitReserverGenome, double> fitnessFunc = g =>
            {
                var x = optimalX - g.BlastX;
                var y = optimalY - g.BlastY;
                var z = optimalAlt - g.BlastAlt;
                var distFromOptimalPositionSquared = x * x + y * y + z * z;
                var normalisedDistFactor = distFromOptimalPositionSquared / maxDistSquared;

                var orientationDist = optimalOrientation - g.BlastOrientation;
                var normalisedOrientationFactor = orientationDist * orientationDist / 180 / 180;

                var scaleDist = optimalScale - g.BlastScale;
                var normalisedScaleDistFactor = scaleDist * scaleDist;

                return 3 - normalisedDistFactor
                       - normalisedOrientationFactor
                       - normalisedScaleDistFactor;
            };

            Func<PitReserverGenome> genomeInstantiator = () => new PitReserverGenome();

            var bestResult = SolveBestResultWithStatLogging(chromosomes, genomeInstantiator, fitnessFunc);

            Assert.IsNotNull(bestResult);
            Assert.AreEqual(optimalX, bestResult.BlastX, 100);
            Assert.AreEqual(optimalY, bestResult.BlastY, 100);
            Assert.AreEqual(optimalAlt, bestResult.BlastAlt, 100);
            Assert.AreEqual(optimalOrientation, bestResult.BlastOrientation, 1);
            Assert.AreEqual(optimalScale, bestResult.BlastScale, .01);
        }

        [TestMethod]
        public void TestSolveForOreDesposits()
        {
            var oreDeposits = new[]
            {
                //new OreDeposit { X = 100, Y = 640, Alt = 10 },
                //new OreDeposit { X = 1100, Y = 560, Alt = -20 },
                //new OreDeposit { X = 320, Y = -3780, Alt = 50 },
                //new OreDeposit { X = -1880, Y = -210, Alt = -180 },
                //new OreDeposit { X = 1200, Y = -1100, Alt = 220 },
                new OreDeposit { X = -300, Y = 700, Alt = -26 },
                new OreDeposit { X = 900, Y = -2100, Alt = 52 },
            };

            var chromosomes = new[]
            {
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastX,
                    (g,d) => g.BlastX = d,
                    () => ThreadLocalRandom.NextDouble() * 10000 - 5000
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastY,
                    (g,d) => g.BlastY = d,
                    () => ThreadLocalRandom.NextDouble() * 10000 - 5000
                    ),
                new MemberChromosome<PitReserverGenome, double>(
                    g => g.BlastAlt,
                    (g,d) => g.BlastAlt = d,
                    () => ThreadLocalRandom.NextDouble() * 500 - 250
                    ),
                //new MemberChromosome<PitReserverGenome, double>(
                //    g => g.BlastOrientation,
                //    (g,d) => g.BlastOrientation = d,
                //    () => ThreadLocalRandom.NextDouble() * 360
                //    ),
                //new MemberChromosome<PitReserverGenome, double>(
                //    g => g.BlastScale,
                //    (g,d) => g.BlastScale = d,
                //    ThreadLocalRandom.NextDouble
                //    ),
            };
            Func<PitReserverGenome, double> fitnessFunc = g =>
            {
                var oreDistanceSquareSum = (
                    from deposit in oreDeposits
                    let x = deposit.X - g.BlastX
                    let y = deposit.Y - g.BlastY
                    let z = deposit.Alt - g.BlastAlt
                    select (x * x) + (y * y) + (z * z)
                    ).Sum();

                return -oreDistanceSquareSum;
            };

            Func<PitReserverGenome> genomeInstantiator = () => new PitReserverGenome();

            var bestResult = SolveBestResultWithStatLogging(chromosomes, genomeInstantiator, fitnessFunc);

            Assert.IsNotNull(bestResult);
            //Assert.AreEqual(optimalX, bestResult.BlastX, 1);
        }

        [TestMethod]
        public void TestSolveForString()
        {
            const string goalString = "Teenage Mutant Ninja Turtles 0123456789";
            var goalChars = goalString.ToCharArray();
            var possibleCharValues = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToCharArray();

            var chromosomes = from i in Enumerable.Range(0, goalChars.Length)
                              let c = goalChars[i]
                              select new MemberChromosome<CharStringGenome, char>(
                                  g => g.Chars[i],
                                  (g, d) => g.Chars[i] = d,
                                  () => possibleCharValues.Pick(ThreadLocalRandom.NextDouble())
                                  );

            Func<CharStringGenome> genomeInstantiator = () =>
                new CharStringGenome
                {
                    Chars = new char[goalChars.Length],
                };

            Func<CharStringGenome, double> fitnessFunc = g =>
            {
                var wrongChars = goalChars.Where((c, i) => g.Chars[i] != c).Count();
                return -wrongChars;
            };

            Func<CharStringGenome, bool> successTester = (g) => (int)g.Fitness == 0;

            var bestResult = SolveBestResultWithStatLogging(chromosomes, genomeInstantiator, fitnessFunc, successTester);

            var bestResultString = new string(bestResult.Chars);
            Assert.AreEqual(goalString, bestResultString);
        }

        private static TGenome SolveBestResultWithStatLogging<TGenome>(IEnumerable<IChromosome<TGenome>> chromosomes, Func<TGenome> genomeInstantiator, Func<TGenome, double> fitnessFunc, Func<TGenome,bool> optionalSuccessTester = null, int populationSize = 1000, int solveIterations = 100)
            where TGenome : class, IGenome, new()
        {
            var service = new SolverService<TGenome>
            {
                GenomeInstantiator = genomeInstantiator,
                Chromosomes = chromosomes,
                Crosser = new RandomChanceEitherWithMutation<TGenome>(.05),
                Selector = Selectors.CubedWeighted,
            };

            TGenome bestResult = null, prevResult = null;
            var iterator = service.Solve(populationSize, fitnessFunc).GetEnumerator();
            for (var i = 0; i < solveIterations; i++)
            {
                iterator.MoveNext();

                var population = iterator.Current;
                var averageFitness = population.Average(g => g.Fitness);

                var thisResult = population[0];
                
                if (bestResult == null || bestResult.Fitness < thisResult.Fitness)
                {
                    bestResult = thisResult;
                }

                var fitnessDelta = prevResult == null ? 0 : thisResult.Fitness - prevResult.Fitness;
                var fitnessDeltaPc = prevResult == null ? 100 : fitnessDelta / prevResult.Fitness * 100;
                Console.WriteLine(
                    "Iteration: {0,5:0} This: {1:0.000} Average: {2:0.000} Best: {3:0.000} ({4:-;+0.00000;-0.00000}%) {5}",
                    i + 1,
                    thisResult.Fitness,
                    averageFitness,
                    bestResult.Fitness,
                    fitnessDeltaPc,
                    bestResult);

                if (optionalSuccessTester != null && optionalSuccessTester(iterator.Current[0]))
                {
                    return iterator.Current[0];
                }

                prevResult = thisResult;
            }
            return bestResult;
        }
    }
}
