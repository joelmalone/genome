﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genome.Tests
{

    public class PitReserverGenome : IGenome
    {
        public double BlastOrientation { get; set; }
        public double BlastScale { get; set; }
        public double BlastX { get; set; }
        public double BlastY { get; set; }
        public double BlastAlt { get; set; }

        public double Fitness { get; set; }

        public override string ToString()
        {
            return String.Format("X: {0:0.00} Y: {1:0.00} Alt: {2:0.00}", BlastX, BlastY, BlastAlt);
        }
    }

    public class OreDeposit
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Alt { get; set; }
    }

}
