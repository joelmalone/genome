﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genome.Tests
{
    public class CharStringGenome : IGenome
    {
        public double Fitness { get; set; }

        // TODO: hardcoded length is dumb - better to pass a spawnFunc to the solver
        public char[] Chars { get; set; }

        public override string ToString()
        {
            return new string(Chars);
        }
    }
}
